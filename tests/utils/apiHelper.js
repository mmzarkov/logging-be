const axios = require('axios');
const config = require('config');

// Initiate axios instance
const PORT = config.get('api.port');
const apiVersion = config.get('api.version');
const instance = axios.create({
  baseURL: `http://localhost:${PORT}${apiVersion}`,
  timeout: 10000,
});

module.exports = instance;
