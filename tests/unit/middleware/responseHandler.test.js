/* eslint-disable global-require */
/* eslint-disable no-undef */
const assert = require('assert');
const sinon = require('sinon');
const mockery = require('mockery');

describe('Unit test for response handler', () => {
  const uuid = 'ca64ae87-8398-4ca7-a68f-7ae49b90dd27';
  let sandbox;
  let nextSpy;
  let respHandler;
  let uuidStub;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    nextSpy = sandbox.spy();

    const BasePublicErrorMock = Error;
    const loggerMock = {
      infoWithContext: () => {},
    };

    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
    });

    uuidStub = sandbox.stub().returns(uuid);
    mockery.registerMock('uuid.v4', uuidStub);

    mockery.registerMock('../../logging/logger', loggerMock);
    mockery.registerMock('../../errors/baseError', BasePublicErrorMock);

    respHandler = require('../../../lib/server/middleware/responseHandler')
      .init;
  });

  afterEach(() => {
    sandbox.restore();
    mockery.deregisterAll();
    mockery.disable();
  });

  it('OK case - response hanbler should be called successfully', async () => {
    const fakeContext = {
      state: {
        requestInitTime: 'requestInitTime',
        requestId: 'requestId',
      },
      request: {
        headers: {},
      },
      response: {
        type: 'application/json',
      },
    };

    await respHandler(fakeContext, nextSpy);
    assert.ok(nextSpy.calledOnce);
  });

  it('Error case', async () => {
    const fakeContext = {
      response: {},
      app: {
        emit: () => {},
      },
    };

    try {
      await respHandler(fakeContext);
      assert.fail();
    } catch (err) {
      assert.ok(err);
      assert.equal(
        err.message,
        "Cannot read property 'requestInitTime' of undefined"
      );
    }
  });
});
