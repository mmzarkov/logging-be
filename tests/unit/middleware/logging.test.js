/* eslint-disable global-require */
/* eslint-disable no-undef */
const mockery = require('mockery');
const assert = require('assert');
const sinon = require('sinon');

describe('Unit tests for middleware - logging function', () => {
  //   let loggerStub;
  let logging;
  let sandbox;
  let nextSpy;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    nextSpy = sandbox.spy();

    const loggerMock = {
      infoWithContext: () => {},
    };

    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
    });

    mockery.registerMock('../../logging/logger', loggerMock);
    logging = require('../../../lib/server/middleware/logging').init;
  });

  afterEach(() => {
    sandbox.restore();
    mockery.deregisterAll();
    mockery.disable();
  });

  it('OK case - logger function should be called successfully', async () => {
    const mockContext = {
      url: 'url',
      originalUrl: 'url',
      request: {
        method: 'GET',
      },
      header: {
        referer: 'refer',
        ip: '127.0.0.1',
      },
    };

    try {
      await logging(mockContext, nextSpy);
      assert.ok(nextSpy.calledOnce);
    } catch (err) {
      assert.fail();
    }
  });

  it('OK case - logger function should be called successfully with default values', async () => {
    const mockContext = {
      url: 'url',
      request: {
        method: '',
      },
      header: {
        referer: 'refer',
        ip: '',
      },
    };
    try {
      await logging(mockContext, nextSpy);
      assert.ok(nextSpy.calledOnce);
    } catch (err) {
      assert.fail();
    }
  });

  it('Error case - missing required property \'method\'', async () => {
    const fakeContext = {
      url: 'url',
      originalUrl: 'url',
      header: {
        ip: '127.0.0.1',
      },
    };

    try {
      await logging(fakeContext, nextSpy);
      assert.ok(nextSpy.calledOnce);
    } catch (err) {
      assert.ok(err);
      assert.equal(err.message, "Cannot read property 'method' of undefined");
    }
  });
});
