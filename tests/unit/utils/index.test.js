/* eslint-disable no-undef */
// This test should be updated !!!
const assert = require('assert');
const {
  bodyToJSON,
  stringify,
  objectAssign,
} = require('./../../../lib/utils/index');

describe('Unit test for utils - bodyToJSON', () => {
  const testData = [
    {
      description: 'String as a body parameter should be converted to JSON',
      body: '{ "name":"John"}',
      expectedResponse: { name: 'John' },
    },
    {
      description: 'JSON as body parameter shouldnt be converted',
      body: { name: 'John' },
      expectedResponse: { name: 'John' },
    },
    {
      description: 'Empty input - should be converted to the JSON',
      body: '',
      expectedResponse: {},
    },
  ];

  testData.forEach(data => {
    it(`${data.description}`, () => {
      const response = bodyToJSON(data.body);
      assert.equal(response.name, data.expectedResponse.name);
      assert.equal(typeof response, 'object');
    });
  });
});

describe('Unit test for utils - stringify', () => {
  const testData = [
    {
      description: 'JSON should be converted to the string',
      body: { name: 'stringify' },
      expectedResponse: '{"name":"stringify"}',
    },
    {
      description:
        'Empty input - should be converted to the string',
      body: '',
      expectedResponse: '{}',
    },
  ];

  testData.forEach(data => {
    it(`${data.description}`, () => {
      const response = stringify(data.body);
      assert.equal(response, data.expectedResponse);
      assert.equal(typeof response, 'string');
    });
  });
});

describe('Unit test for utils - objectAssign', () => {
  const device = {
    id: 1,
    name: 'device',
  };

  const response = objectAssign(device, { name: 'new device' });
  assert.equal(typeof response, 'object');
  assert.equal(response.name, 'new device');
});
