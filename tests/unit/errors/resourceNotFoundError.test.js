/* eslint-disable no-undef */
const assert = require('assert');
const ResourceNotFoundError = require('../../../lib/errors/resourceNotFoundError');

describe('Unit test for operation failed error', () => {
  const resourceId = '3cbc7fe9-a633-4f6b-8215-b7e27fa466c0';

  it('builds an error with its valid status', () => {
    const errResponse = new ResourceNotFoundError(resourceId);
    assert.equal(errResponse.data.resource, resourceId);
    assert.equal(errResponse.message, `Resource ${resourceId} was not found.`);
  });
});
