/* eslint-disable no-undef */
const assert = require('assert');
const ValidationError = require('../../../lib/errors/validationError');

describe('Unit test for validation error', () => {
  const errData = [
    {
      error: 'Bad request',
      statusCode: 400,
    },
    {
      error: 'Conflict',
      statusCode: 409,
    },
  ];

  errData.forEach(data => {
    it(`${data.error} builds an error with its valid status - ${data.statusCode}`, () => {
      const errResponse = new ValidationError(data.error, {
        statusCode: data.statusCode,
      });
      assert.equal(errResponse.statusCode, data.statusCode);
    });
  });

  it('builds an error with default status code 400', () => {
    const errResponse = new ValidationError('Validation error');
    assert.equal(errResponse.statusCode, 400);
  });
});
