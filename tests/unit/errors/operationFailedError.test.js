/* eslint-disable no-undef */
const assert = require('assert');
const OperationFailedError = require('./../../../lib/errors/operationFailedError');

describe('Unit test for operation failed error', () => {
  const errData = [
    {
      error: 'Internal Server Error',
      statusCode: 500,
    },
    {
      error: 'Service Unavailable',
      statusCode: 503,
    },
  ];

  errData.forEach(data => {
    it(`${data.error} builds an error with its valid status - ${data.statusCode}`, () => {
      const errResponse = new OperationFailedError(data.error, {
        statusCode: data.statusCode,
      });
      assert.equal(errResponse.statusCode, data.statusCode);
    });
  });

  it('builds an error with default status code 500', () => {
    const errResponse = new OperationFailedError('Error');
    assert.equal(errResponse.statusCode, 500);
  });
});
