/* eslint-disable no-undef */
const assert = require('assert');
const server = require('../../lib/server');
const apiInstance = require('../utils/apiHelper');

describe('Integration test for /health endpoint', async () => {
  let app;
  before(async () => {
    app = await server.start();
  });

  after(async () => {
    app.close();
  });

  it('should return 200 when server is up', async () => {
    const { error, status, data } = await apiInstance.get('/health');

    assert.ok(!error);
    assert.deepEqual(status, 200);
    assert.deepEqual(typeof data, 'object');
    assert.deepEqual(data, { success: true });
  });
});
