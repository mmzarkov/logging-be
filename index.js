const server = require('./lib/server/index');

/**
 * Entry script
 * Calls ./lib/server/index.js which initializes the koa.js servers
 */
process.on('unhandledRejection', err => {
  // eslint-disable-next-line no-console
  console.log(err);
  process.exit(1);
});

server.start();
