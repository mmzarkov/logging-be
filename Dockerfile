FROM node:10-alpine

## Set default app directory and port
ENV PROJECT_DIR=/app \
    PORT=80

WORKDIR ${PROJECT_DIR}

COPY package*.json ${PROJECT_DIR}/

## Install app dependencies for production
RUN npm install --production && \
  npm cache clean --force

## Bundle app source
COPY . ${PROJECT_DIR}

## Install curl needed for healtcheck
RUN apk update && \
	apk add --no-cache curl

## Expose port
EXPOSE ${PORT}

## Healtcheck
HEALTHCHECK CMD curl --fail http://localhost:${PORT}/health || exit 1

CMD [ "node", "index.js" ]