/**
 * Converts a JSON to js object. The function also checks validity of the supplied `body`.
 * If typeof `body` === Object. no conversion will be performed
 * @param {any} body The body that needs to be transformed into a JS Object
 */
const bodyToJSON = body => {
  if (!body) {
    return {};
  }
  if (typeof body === 'object') {
    return body;
  }
  return JSON.parse(body);
};

const stringify = body => {
  if (!body) {
    return JSON.stringify({});
  }
  return JSON.stringify(body);
};

const objectAssign = (...params) => Object.assign(...params);

module.exports = {
  bodyToJSON,
  stringify,
  objectAssign,
};
