const { transports, createLogger, format } = require('winston');
const config = require('config');

const parseContext = ctx => {
  if (!ctx.state) {
    return {};
  }
  const state = ctx.state || {};
  const metaFields = {
    requestId: state.requestId,
  };
  return metaFields;
};

const logger = createLogger({
  level: config.get('logger.level'),
  format: format.combine(format.timestamp(), format.json()),
  transports: [new transports.Console()],
});

/**
 * Log new ERROR message
 * @param {String} message
 * @param {any} err defaults to Object{}
 */
const error = (message, err = {}) => {
  logger.error(
    `${message}. Error: ${err.status || 500} - ${err.message} - ${err.stack}`
  );
};

/**
 * Log new ERROR level message with fields from koa context
 * @param {String} message
 * @param {Object} ctx A Koa context object to get fields from
 */
const errorWithContext = (message, err = {}, ctx = {}) => {
  logger.error(
    `${message}. Error: ${err.status || 500} - ${err.message} - ${err.stack}`,
    parseContext(ctx)
  );
};

/**
 * Log new debug message
 * @param {String} message
 * @param {Object} metaFields An object that contains additional fields to be logged
 */
const debug = (message, metaFields) => {
  logger.debug(message, metaFields);
};

/**
 * Log new INFO level message
 * @param {String} message
 * @param {Object} metaFields An object that contains additional fields to be logged
 */
const info = (message, metaFields) => {
  logger.info(message, metaFields);
};

/**
 * Log new INFO level message
 * @param {String} message
 * @param {Object} ctx A Koa context object to get fields from
 */
const infoWithContext = (message, ctx = {}) => {
  logger.info(message, parseContext(ctx));
};

/**
 * Log new warning message
 * @param {String} message
 * @param {Object} metaFields An object that contains additional fields to be logged
 */
const warn = (message, metaFields) => {
  logger.warn(message, metaFields);
};

/**
 * The exported methods override the default winston implementation
 */
module.exports = {
  error,
  warn,
  info,
  debug,
  infoWithContext,
  errorWithContext,
};
