const uuid = require('uuid').v4;

const BasePublicError = require('../../errors/baseError');
const logger = require('../../logging/logger');

async function init(ctx, next) {
  ctx.response.type = 'application/json';
  try {
    ctx.state.requestInitTime = Date.now();
    ctx.state.requestId = uuid();

    await next();
  } catch (err) {
    let errMsg = '';
    let status = 500;

    if (err instanceof BasePublicError) {
      status = err.statusCode;
      errMsg = err.message;
    } else {
      errMsg = `Internal server error for request with id ${ctx.state.requestId}`;
      status = 500;
    }
    ctx.response.status = status || 500;
    ctx.body = { error: errMsg };
    ctx.app.emit('error', err, ctx);
  }
  // Log the request response
  const respTime = Date.now() - ctx.state.requestInitTime;
  const loggingMessage = `| Status ${ctx.status} Response Time: ${respTime} ms`;
  logger.infoWithContext(loggingMessage, ctx);
}

module.exports = {
  init,
};
