const logger = require('../../logging/logger');

async function init(ctx, next) {
  const { header } = ctx;
  const referer = header.referer ? header.referer : false;
  const refererMessage = referer ? ` referer: ${referer} ` : '';
  const url = ctx.originalUrl ? ctx.originalUrl : ctx.url;
  const method = ctx.request.method || 'GET';
  const ipAddress = header.ip || '::1';
  const userAgent = header['user-agent'] || 'User agent is unavailable';
  // TODO remove the query params values from the log as they might include sen sitive information
  const loggingMessage = `| ${method} > ${url} ${refererMessage} | ${ipAddress} | User-Agent ${userAgent}`;
  logger.infoWithContext(loggingMessage, ctx);
  await next();
}

module.exports = {
  init,
};
