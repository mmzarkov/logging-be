const axios = require('axios');
const FormData = require('form-data');
const config = require('config');

const authService = config.get('authn.baseUrl');
const authPort = config.get('authn.port');

function routes(router) {
  router.post('/signup', async (ctx, next) => {
    let response;
    const { firstname, lastname, username, password } = ctx.request.body;

    try {
      const formData = new FormData();
      formData.append('firstname', firstname);
      formData.append('lastname', lastname);
      formData.append('username', username);
      formData.append('password', password);

      response = await axios.post(
        `${authService}:${authPort}/accounts`,
        formData,
        {
          headers: formData.getHeaders(),
        }
      );
    } catch (error) {
      ctx.throw(422, error.message);
    }

    const token = `${response.data.result.id_token}`;

    ctx.body = {
      token
    };


    ctx.set('authn', token);
    ctx.set('Access-Control-Expose-Headers', 'Set-Cookie, authn');
    ctx.cookies.set('authn', token, { maxAge: 900000, httpOnly: true });
    ctx.status = 302;
    ctx.redirect(`http://localhost:8000`);

    await next();
  });
}

module.exports = {
  routes,
};
