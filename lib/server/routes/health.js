/**
 * Registers all related routes
 * All routes are prefixed - check the routes configuration in lib/server/routes/index.js
 */
function routes(router) {
  router.get('/health', async (ctx, next) => {

    ctx.body = {
      success: true,
    };

    await next();
  });
}

module.exports = {
  routes,
};
