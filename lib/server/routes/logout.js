const axios = require('axios');
const config = require('config');

const authService = config.get('authn.baseUrl');
const authPort = config.get('authn.port');

function routes(router) {
  router.delete('/logout', async (ctx, next) => {
    try {
      await axios.delete(`${authService}:${authPort}/session`);
    } catch (error) {
      ctx.throw(500, error.message);
    }

    ctx.response.status = 204;
    await next();
  });
}

module.exports = {
  routes,
};
