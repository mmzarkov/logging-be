const config = require('config');
const Router = require('koa-router');
const health = require('./health');
const signup = require('./signup');
const login = require('./login');
const logout = require('./logout');

const prefix = config.get('api.version');

/**
 * This module registers all routes enabled in the application
 * All routes for a resource should be added as a separate file
 * Request logging is handled by ../middleware/logger
 * @param {Object} app koa application
 */
function register(app) {
  const vOneRouter = new Router({
    prefix,
  });

  // Register routes
  health.routes(vOneRouter);
  signup.routes(vOneRouter);
  login.routes(vOneRouter);
  logout.routes(vOneRouter);

  // Initialize the router
  app.use(vOneRouter.routes()).use(vOneRouter.allowedMethods());
}

module.exports = register;
