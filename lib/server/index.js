/* eslint-disable consistent-return */
const config = require('config');
const app = require('./app');
const logger = require('../logging/logger');

const PORT = config.get('api.port');

const serverInitialization = async () => {
  try {
    logger.info('| Koa | Server is starting');
    const server = app.listen(PORT);

    server.on('close', () => {
      logger.info('| Koa | Server closed');
    });
    logger.info(`| Koa | Application listening on port ${PORT}`);

    return server;
  } catch (err) {
    logger.error(`Can not start the server: ${err}`);
    process.exit(1);
  }
};

module.exports = {
  start: serverInitialization,
};
