const Koa = require('koa');
const koaBody = require('koa-bodyparser');
const session = require('koa-session');
const CSRF = require('koa-csrf');
const cors = require('@koa/cors');
const config = require('config');
const uuid = require('uuid');

const loggerMw = require('./middleware/logging');
const responseHandlerMw = require('./middleware/responseHandler');

const routes = require('./routes');
const logger = require('../logging/logger');

// Create the app
const app = new Koa();

// IMPORTANT: the order of the below app.use calls is very important
// response handler should be called first as it handles the try catch
// logic for the whole request
// Assign keys to be used for cookie signing
// TODO this needs to come from somewhere else and be static
// otherwise every restart of the server will invalidate the client cookies
// due to signing key change
app.keys = ['session key', uuid()];

// Request init and error handling
app.use(responseHandlerMw.init);

// Attach the logger middleware
app.use(loggerMw.init);

// Register the session middleware
app.use(session(app));

// Register the koa body parser middleware
app.use(koaBody());

// Register the koa cors middleware
app.use(cors({
  origin: '*',
  credentials: true
}));

// If enabled, register the CRSF middleware
if (
  config.has('security.enableCSRF') &&
  config.get('security.enableCSRF') === true
) {
  app.use(new CSRF());
}

// Register routes
routes(app);

// Listener to centralise error logging
app.on('error', (err, ctx) => {
  logger.errorWithContext('| Error while processing request | ', err, ctx);
});

module.exports = app;
