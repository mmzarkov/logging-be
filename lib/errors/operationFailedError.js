const BaseError = require('./baseError');
const utils = require('../utils');

class OperationFailedError extends BaseError {
  constructor(message, options = {}) {
    super(`Operation failed. ${utils.stringify(message)}`);
    this.statusCode = options.statusCode || 500;
  }
}

module.exports = OperationFailedError;
