const BaseError = require('./baseError');

class ValidationError extends BaseError {
  constructor(message, options = {}) {
    super(`Data validation failed. ${message}`);
    this.statusCode = options.statusCode || 400;
  }
}

module.exports = ValidationError;
