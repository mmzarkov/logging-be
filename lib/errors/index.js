const ValidationError = require('./validationError');
const ResourceNotFoundError = require('./resourceNotFoundError');
const OperationFailedError = require('./operationFailedError');

module.exports = {
  ValidationError,
  ResourceNotFoundError,
  OperationFailedError,
};
