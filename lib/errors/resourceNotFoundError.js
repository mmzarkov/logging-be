const BaseError = require('./baseError');

class ResourceNotFoundError extends BaseError {
  constructor(resource, query) {
    super(`Resource ${resource} was not found.`);
    this.data = { resource, query };
  }
}

module.exports = ResourceNotFoundError;
